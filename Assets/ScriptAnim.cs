using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptAnim : MonoBehaviour
{
    [SerializeField] Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator.SetTrigger("spin");
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void setJump()
    {
        animator.SetTrigger("jump");
    }
    public void setExit()
    {
        animator.SetTrigger("exit");
    }
}
