using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviromentDmg : MonoBehaviour
{
    [SerializeField] float AttackRange = 5f;
    [SerializeField] int AttackDamage = 3;
    [SerializeField] float Delay = 0;
    
    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, AttackRange);

    }
    void OnTriggerEnter2D(Collider2D entity)
    {
        if(entity.gameObject.tag == "Player")
        {
            Damage(AttackDamage, entity.gameObject.GetComponent<PlayerHealth>());
        }
    }
    void OnTriggerStay2D(Collider2D entity)
    {
        if (entity.gameObject.tag == "Player")
        {
            Damage(AttackDamage, entity.gameObject.GetComponent<PlayerHealth>());
        }
    }
    void Damage(int damage, PlayerHealth hp)
    {
            hp.TakeDamage(damage);
            Delay = Time.time + 1f / 2f;
    }
}
