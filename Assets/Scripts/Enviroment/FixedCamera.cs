using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedCamera : MonoBehaviour
{
    [SerializeField] Transform playerPosition;
    [SerializeField] float cameraY = 0.5f;
    // Update is called once per frame
    void Update()
    {
        Vector3 newCameraPosition = new Vector3(playerPosition.position.x, playerPosition.position.y + cameraY, -15);
        gameObject.transform.position = newCameraPosition;  
    }
}
