using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Audio;
public class Menu : MonoBehaviour
{
    [SerializeField] Button launch;
    [SerializeField] Button settings;
    [SerializeField] Button quit;
    [SerializeField] GameObject panel;
    [SerializeField] Dropdown resolutionDropDown;
    Resolution[] resolutions;
    [SerializeField] Toggle vsync;
    [SerializeField] Dropdown qualityDropDown;
    [SerializeField] AudioMixer audimixer;
    public void hidePanel()
    {
        panel.SetActive(false);
    }
    public void ShowPanel()
    {
        panel.SetActive(true);
    }
    public void LaunchGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropDown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && 
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropDown.AddOptions(options);
        resolutionDropDown.value = currentResolutionIndex;
        resolutionDropDown.RefreshShownValue();
        panel.SetActive(false);
        if(QualitySettings.vSyncCount == 0)
        {
            vsync.isOn = false;
        }
        else
        {
            vsync.isOn = true;
        }
    }
    public void doExitGame()
    {
        Application.Quit();
    }
    public void MainMenuButtonSound()
    {
        AudioManager.Instance.PlaySFX("Main Menu Click");
    }
    public void SetResolution(int index)
    {
        var res = Screen.resolutions[index];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }
    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
    public void SetQuality(int QualityIndex)
    {
        QualitySettings.SetQualityLevel(QualityIndex);
    }
    public void SetVsync()
    {
        if (vsync.isOn)
        {
            QualitySettings.vSyncCount = 1;
            //Debug.Log("AAAAAA");
        }
        else
        {
            QualitySettings.vSyncCount = 0;
        }
    }
    void SetVolume (float volume)
    {
        audimixer.SetFloat("MasterVolume", Mathf.Log10(volume)*20);
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.F12))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        }
    }
}
