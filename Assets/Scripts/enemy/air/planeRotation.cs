using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class planeRotation : MonoBehaviour
{
    public AIPath aiPath;
    [SerializeField] SpriteRenderer sr;
    // Update is called once per frame
    void Update()
    {
        if (aiPath.desiredVelocity.x >= 0.01f)
        {
            sr.flipX = false;
        }
        if (aiPath.desiredVelocity.x <= -0.01f)
        {
            sr.flipX = true;
        }
    }
}