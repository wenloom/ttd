using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Rigidbody2D rb;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] Animator animator;
    [SerializeField] GameObject go;
    [SerializeField] float speed = 5f;
    [SerializeField] float jumpSpeed = 14f;
    [SerializeField] BoxCollider2D collider;
    [SerializeField] int DashRange = 20;
    private bool canJump = true;
    bool isDashed = false;
    bool IsJump = false;
    void Update()
    {
        if(Physics2D.Linecast(transform.position, go.GetComponent<Transform>().position, 1 << LayerMask.NameToLayer("Ground")))
        {
            isDashed = false;
            canJump = true;
            IsJump = false;
            //Debug.Log("CANJUMP");
        }
        else
        {
            canJump = false;
            IsJump = true;
        }
        if (Input.GetKey(KeyCode.D) && !isDashed)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            animator.SetBool("isRunning", true);
            sr.flipX = false;
        }
        else if (Input.GetKey(KeyCode.A) && !isDashed)
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            animator.SetBool("isRunning", true);
            sr.flipX = true;
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
        if (Input.GetKeyDown(KeyCode.Space) && canJump == true && !isDashed && IsJump == false)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            canJump = false;
            IsJump = true;
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);

            canJump = false;
            IsJump = false;
        }
        if (Input.GetKeyDown(KeyCode.C) && !isDashed) //&& canJump == false && canUp == true)
        {
            collider.offset = new Vector3(-2.235174e-08f, -0.06883941f, 0);
            collider.size = new Vector3(0.1982492f, 0.4523212f, 0);
            animator.SetBool("isCrouching", true);
            Debug.Log("CROUCH");
        }
        else if (Input.GetKeyUp(KeyCode.C)) //&& canJump == false && canUp == true)
        {
            collider.offset = new Vector3(-2.235174e-08f, 0, 0);
            collider.size = new Vector3(0.1982492f, 0.64f, 0);
            animator.SetBool("isCrouching", false);
            Debug.Log("CROUCH");
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && IsJump == true)
        {
            if (IsJump == true) 
            { 
                Dash(); 
            }
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            animator.SetBool("isDashing", false);
            collider.offset = new Vector3(-2.235174e-08f, 0, 0);
            collider.size = new Vector3(0.1982492f, 0.64f, 0);
        }
    }
    void Dash()
    {
        if (!isDashed)
        {
            isDashed = true;
            animator.SetBool("isDashing", true);
            collider.offset = new Vector3(2.980232e-08f, -0.0007918775f, 0);
            collider.size = new Vector3(0.639793f, 0.1545362f, 0);
            if (sr.flipX == true)
            {
                rb.velocity = new Vector2(-0.8f * DashRange, 0);
            }
            else
            {
                rb.velocity = new Vector2(0.8f * DashRange, 0);
            }
        }
    }
}
